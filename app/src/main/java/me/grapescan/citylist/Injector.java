package me.grapescan.citylist;

import me.grapescan.citylist.model.CityRepository;
import me.grapescan.citylist.model.InMemoryCityRepository;
import me.grapescan.citylist.ui.list.ListContract;
import me.grapescan.citylist.ui.list.ListPresenter;

public class Injector {

    private static ListContract.Presenter listPresenter;
    private static CityRepository cityRepository;

    public static CityRepository getCityRepository() {
        if (cityRepository == null) {
            cityRepository = new InMemoryCityRepository();
        }
        return cityRepository;
    }

    public static ListContract.Presenter getListPresenter() {
        if (listPresenter == null) {
            listPresenter = new ListPresenter(getCityRepository());
        }
        return listPresenter;
    }

}

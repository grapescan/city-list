package me.grapescan.citylist.ui.list;

import android.os.Handler;
import android.os.Looper;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import me.grapescan.citylist.model.City;
import me.grapescan.citylist.model.CityRepository;

public class ListPresenter implements ListContract.Presenter {

    private ListContract.View view = ListContract.viewStub;
    private ExecutorService backgroundExecutor = Executors.newCachedThreadPool();
    private Handler mainThreadHandler = new Handler(Looper.getMainLooper());
    private final CityRepository repo;

    public ListPresenter(CityRepository repo) {
        this.repo = repo;
    }

    @Override
    public void onViewAttach(ListContract.View view) {
        this.view = view;
        backgroundExecutor.submit(() -> repo.getAllCities(this::showCityList));
    }

    @Override
    public void onViewDetach() {
        view = ListContract.viewStub;
    }

    @Override
    public void onSearchClose() {
        backgroundExecutor.submit(() -> repo.getAllCities(this::showCityList));
    }

    @Override
    public void onSearchQueryUpdate(String query) {
        backgroundExecutor.submit(() -> repo.filterCities(query, this::showCityList));
    }

    private void showCityList(List<City> cities) {
        mainThreadHandler.post(() -> view.showCityList(cities));
    }
}

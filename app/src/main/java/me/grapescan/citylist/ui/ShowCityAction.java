package me.grapescan.citylist.ui;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import java.util.HashMap;
import java.util.Map;

import me.grapescan.citylist.App;
import me.grapescan.citylist.model.City;

public class ShowCityAction {
    private final static String ACTION_SHOW_CITY = "me.grapescan.citylist.ACTION_SHOW_CITY";
    private final static String EXTRA_CITY = "me.grapescan.citylist.EXTRA_CITY";
    private final static IntentFilter FILTER = new IntentFilter(ACTION_SHOW_CITY);

    private final static Map<Listener, BroadcastReceiver> listeners = new HashMap<>();

    public static void registerListener(Listener listener) {
        BroadcastReceiver receiver = new CityReceiver(listener);
        listeners.put(listener, receiver);
        LocalBroadcastManager.getInstance(App.context).registerReceiver(receiver, FILTER);
    }

    public static void unregisterListener(Listener listener) {
        BroadcastReceiver receiver = listeners.get(listener);
        LocalBroadcastManager.getInstance(App.context).unregisterReceiver(receiver);
    }

    public static void send(City city) {
        Intent intent = new Intent(ACTION_SHOW_CITY);
        intent.putExtra(EXTRA_CITY, city);
        LocalBroadcastManager.getInstance(App.context).sendBroadcast(intent);
    }

    public interface Listener {
        void onShowCity(City city);
    }

    private static class CityReceiver extends BroadcastReceiver {
        private Listener listener;

        CityReceiver(Listener listener) {
            this.listener = listener;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            City city = (City) intent.getSerializableExtra(EXTRA_CITY);
            listener.onShowCity(city);
        }
    }
}

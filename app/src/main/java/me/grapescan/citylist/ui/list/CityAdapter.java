package me.grapescan.citylist.ui.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import me.grapescan.citylist.R;
import me.grapescan.citylist.model.City;

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.CityViewHolder> {

    interface OnCityClickListener {
        void onClick(City city);
    }

    private List<City> data;
    private final OnCityClickListener listener;

    CityAdapter(List<City> items, OnCityClickListener listener) {
        data = items;
        this.listener = listener;
    }

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city, parent, false);
        return new CityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CityViewHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<City> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    class CityViewHolder extends RecyclerView.ViewHolder {
        private final TextView title;

        CityViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.city_item_title);
        }

        void bind(City city) {
            title.setText(city.displayName);
            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onClick(city);
                }
            });
        }
    }
}

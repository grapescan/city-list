package me.grapescan.citylist.ui.list;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.List;

import me.grapescan.citylist.Injector;
import me.grapescan.citylist.R;
import me.grapescan.citylist.model.City;
import me.grapescan.citylist.ui.ShowCityAction;

public class ListFragment extends Fragment implements ListContract.View {

    private final CityAdapter listAdapter = new CityAdapter(Collections.emptyList(), ShowCityAction::send);
    private final ListContract.Presenter presenter = Injector.getListPresenter();

    public ListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        initToolbar(toolbar);

        RecyclerView list = view.findViewById(R.id.list);
        list.setAdapter(listAdapter);

        return view;
    }

    private void initToolbar(Toolbar toolbar) {
        toolbar.setTitle(R.string.app_name);
        toolbar.inflateMenu(R.menu.main);
        MenuItem searchItem = toolbar.getMenu().findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnCloseListener(() -> {
            presenter.onSearchClose();
            return false;
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                presenter.onSearchQueryUpdate(newText);
                return true;
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter.onViewAttach(this);
    }

    @Override
    public void onDetach() {
        presenter.onViewDetach();
        super.onDetach();
    }

    @Override
    public void showCityList(List<City> data) {
        listAdapter.setData(data);
    }
}

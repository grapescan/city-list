package me.grapescan.citylist.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import me.grapescan.citylist.R;

public class MainActivity extends AppCompatActivity {

    private final ShowCityAction.Listener showCityListener = city -> {
        SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        addFragment(mapFragment, "map_fragment");
        mapFragment.getMapAsync(map -> showMarker(map, city.latitude, city.longitude, city.name));
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ShowCityAction.registerListener(showCityListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ShowCityAction.unregisterListener(showCityListener);
    }

    private void addFragment(Fragment fragment, String tag) {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.root, fragment)
                .addToBackStack(tag)
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .show(fragment)
                .commit();
    }

    private void showMarker(GoogleMap map, double lat, double lng, String markerTitle) {
        LatLng markerPosition = new LatLng(lat, lng);
        map.addMarker(new MarkerOptions().position(markerPosition).title(markerTitle));
        map.moveCamera(CameraUpdateFactory.newLatLng(markerPosition));
    }
}

package me.grapescan.citylist.ui.list;


import java.util.List;

import me.grapescan.citylist.model.City;

public interface ListContract {

    View viewStub = new ViewStub();

    interface View {
        void showCityList(List<City> data);
    }

    interface Presenter {
        void onViewAttach(View view);

        void onViewDetach();

        void onSearchClose();

        void onSearchQueryUpdate(String query);
    }

    class ViewStub implements View {
        @Override
        public void showCityList(List<City> data) {
            // do nothing
        }
    }
}

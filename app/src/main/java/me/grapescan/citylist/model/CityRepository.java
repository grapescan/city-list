package me.grapescan.citylist.model;


import java.util.List;

public interface CityRepository {
    void getAllCities(Callback callback);

    void filterCities(String prefix, Callback callback);

    interface Callback {
        void onDataLoaded(List<City> data);
    }
}

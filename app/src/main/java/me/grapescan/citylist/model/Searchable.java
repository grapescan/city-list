package me.grapescan.citylist.model;


public interface Searchable {
    String getSearchKey();
}

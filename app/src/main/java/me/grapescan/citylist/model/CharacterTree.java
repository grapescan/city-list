package me.grapescan.citylist.model;

import android.support.v4.util.Pair;

import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

/**
 * Represents a metadata tree to filter sorted list by prefix
 * <p>
 * Root of this tree is a map of character keys and pair values.
 * Key set includes all possible symbols found in first position of list items.
 * Each value consists of two parts:
 * 1. Sublist of source list that includes items starting with the key character
 * 2. Another CharacterTree built for this sublist (includes characters from 2nd position as its keys)
 * <p>
 * This structure is filled recursively until final CharacterMap includes one key and sublist in its value includes one item.
 * <p>
 * To perform list filtering with this structure, we just need to go down through nested CharacterTrees according to characters given in query string.
 */
public class CharacterTree<T extends Searchable> extends TreeMap<Character, Pair<List<T>, CharacterTree>> {

    public CharacterTree(List<T> data) {
        this(data, 0);
    }

    private CharacterTree(List<T> data, int charPosition) {
        if (data.size() > 1) {
            fill(data, charPosition);
        } else if (data.size() == 1 && data.get(0).getSearchKey().length() > charPosition) {
            put(data.get(0).getSearchKey().charAt(charPosition), new Pair<>(data, null));
        }
    }

    private void fill(List<T> data, int charPosition) {
        int startItemIndex = indexOfItemHavingMinLength(data, charPosition + 1);
        if (startItemIndex == -1) return;
        char lastChar = data.get(startItemIndex).getSearchKey().charAt(charPosition);
        char currentChar;
        boolean currentCharDiffers;
        for (int currentItemIndex = startItemIndex + 1; currentItemIndex < data.size(); currentItemIndex++) {
            if (charPosition < data.get(currentItemIndex).getSearchKey().length()) {
                currentChar = data.get(currentItemIndex).getSearchKey().charAt(charPosition);
                currentCharDiffers = currentChar != lastChar;
                if (currentCharDiffers) {
                    List<T> charList = data.subList(startItemIndex, currentItemIndex);
                    CharacterTree<T> charTree = new CharacterTree<>(charList, charPosition + 1);
                    put(lastChar, new Pair<>(charList, charTree));
                    startItemIndex = currentItemIndex;
                    lastChar = currentChar;
                }
            }
        }
        // build subtree for last char
        List<T> charList = data.subList(startItemIndex, data.size());
        CharacterTree<T> charTree = new CharacterTree<>(charList, charPosition + 1);
        put(lastChar, new Pair<>(charList, charTree));
    }

    private int indexOfItemHavingMinLength(List<T> data, int minLength) {
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getSearchKey().length() >= minLength) {
                return i;
            }
        }
        return -1;
    }

    public List<T> filterBy(String query) {
        if (query != null && query.length() > 0) {
            return filterBy(query.toLowerCase(), 0);
        } else {
            return Collections.emptyList();
        }
    }

    private List<T> filterBy(String query, int charPosition) {
        char currentChar = query.charAt(charPosition);
        if (containsKey(currentChar)) {
            Pair<List<T>, CharacterTree> preResult = get(currentChar);
            if (charPosition == query.length() - 1) {
                return preResult.first;
            } else {
                if (preResult.second == null) {
                    if (preResult.first.get(0).getSearchKey().toLowerCase().startsWith(query.toLowerCase())) {
                        return preResult.first;
                    } else {
                        return Collections.emptyList();
                    }
                } else {
                    return preResult.second.filterBy(query, charPosition + 1);
                }
            }
        } else {
            return Collections.emptyList();
        }
    }
}

package me.grapescan.citylist.model;

import android.support.annotation.NonNull;

import java.io.Serializable;

public class City implements Serializable, Comparable, Searchable {

    public final String country;
    public final String name;
    public final double longitude;
    public final double latitude;
    public final String displayName;
    private final String searchKey;

    public City(String country, String name, double latitude, double longitude) {
        this.country = country;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;

        displayName = name + ", " + country;
        searchKey = displayName.toLowerCase();
    }

    @Override
    public int compareTo(@NonNull Object o) {
        if (o instanceof City) {
            City otherCity = (City) o;
            return searchKey.compareTo(otherCity.searchKey);
        }
        return 0;
    }

    @Override
    public String toString() {
        return displayName;
    }

    @Override
    public String getSearchKey() {
        return searchKey;
    }
}

package me.grapescan.citylist.model;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import me.grapescan.citylist.App;
import me.grapescan.citylist.R;

public class InMemoryCityRepository implements CityRepository {

    private static final String TAG = "InMemoryCityRepository";

    private final List<City> data;
    private final CharacterTree<City> characterTree;

    public InMemoryCityRepository() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(City.class, new CityTypeAdapter())
                .create();
        data = new ArrayList<>(210000);
        InputStream stream = App.context.getResources().openRawResource(R.raw.cities);
        try {
            JsonReader reader = new JsonReader(new InputStreamReader(stream, "UTF-8"));
            reader.beginArray();
            while (reader.hasNext()) {
                City city = gson.fromJson(reader, City.class);
                data.add(city);
            }
            reader.close();
        } catch (UnsupportedEncodingException ex) {
            // do nothing
        } catch (IOException ex) {
            // do nothing
        }
        Collections.sort(data);
        characterTree = new CharacterTree<>(data);
    }

    @Override
    public void getAllCities(Callback callback) {
        callback.onDataLoaded(data);
    }

    @Override
    public void filterCities(String prefix, Callback callback) {
        long startTimestamp = System.currentTimeMillis();
        if (prefix.isEmpty()) {
            callback.onDataLoaded(data);
        }
        List<City> result = characterTree.filterBy(prefix);
        Log.d(TAG, "filtering took " + (System.currentTimeMillis() - startTimestamp) + "ms");
        callback.onDataLoaded(result);
    }
}

package me.grapescan.citylist.model;


import com.google.gson.annotations.SerializedName;

public class Coord {

    @SerializedName("lon")
    public final double longitude;

    @SerializedName("lat")
    public final double latitude;

    public Coord(double longitude, double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }
}

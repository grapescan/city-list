package me.grapescan.citylist.model;


import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

public class CityTypeAdapter extends TypeAdapter<City> {
    @Override
    public void write(JsonWriter out, City value) throws IOException {

    }

    @Override
    public City read(JsonReader in) throws IOException {
        String name = "";
        String country = "";
        double lat = 0;
        double lon = 0;

        in.beginObject();
        while (in.hasNext()) {
            switch (in.nextName()) {
                case "_id":
                    in.skipValue();
                    break;
                case "name":
                    name = in.nextString();
                    break;
                case "country":
                    country = in.nextString();
                    break;
                case "coord":
                    in.beginObject();
                    while (in.hasNext()) {
                        switch (in.nextName()) {
                            case "lat":
                                lat = in.nextDouble();
                                break;
                            case "lon":
                                lon = in.nextDouble();
                                break;
                        }
                    }
                    in.endObject();
                    break;
                default:
                    break;
            }
        }
        in.endObject();

        return new City(country, name, lat, lon);
    }
}

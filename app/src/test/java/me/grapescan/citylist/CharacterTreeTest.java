package me.grapescan.citylist;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import me.grapescan.citylist.model.CharacterTree;
import me.grapescan.citylist.model.City;
import me.grapescan.citylist.model.CityTypeAdapter;

import static org.junit.Assert.assertTrue;

public class CharacterTreeTest {

    @Test
    public void testSimpleSearch() throws Exception {
        List<City> data = new ArrayList<City>() {{
            add(new City("RU", "Moscow", 0, 0));
            add(new City("RU", "Mordor", 0, 0));
            add(new City("RU", "Malinovka", 0, 0));
            add(new City("RU", "Magadan", 0, 0));
            add(new City("RU", "Tula", 0, 0));
            add(new City("RU", "Tver'", 0, 0));
            add(new City("RU", "Samara", 0, 0));
            add(new City("RU", "Saratov", 0, 0));
        }};
        CharacterTree<City> characterTree = new CharacterTree<>(data);
        List<City> result = characterTree.filterBy("ma");
        assertTrue(result.size() == 2);
    }

    @Test
    public void testNonAsciiCharacterSearch() throws Exception {
        List<City> data = new ArrayList<City>() {{
            add(new City("DK", "Årslev", 0, 0));
            add(new City("PL", "Śródmieście", 0, 0));
            add(new City("EE", "Väänikvere", 0, 0));
            add(new City("DK", "Ølby Lyng", 0, 0));
            add(new City("BG", "Асенова махала", 0, 0));
        }};
        CharacterTree<City> characterTree = new CharacterTree<>(data);
        assertTrue(characterTree.filterBy("Å").size() == 1);
        assertTrue(characterTree.filterBy("års").size() == 1);
        assertTrue(characterTree.filterBy("ø").size() == 1);
        assertTrue(characterTree.filterBy("Асенова махала").size() == 1);
    }

    @Test
    public void testInvalidQuerySearch() throws Exception {
        List<City> data = loadRealData();
        CharacterTree<City> characterTree = new CharacterTree<>(data);
        assertTrue(characterTree.filterBy("mmmmm").isEmpty());
        assertTrue(characterTree.filterBy("12345").isEmpty());
        assertTrue(characterTree.filterBy(" ").isEmpty());
        assertTrue(characterTree.filterBy("").isEmpty());
        assertTrue(characterTree.filterBy(null).isEmpty());
    }

    @Test
    public void testRealDataSearch() throws Exception {
        List<City> data = loadRealData();
        CharacterTree<City> characterTree = new CharacterTree<>(data);
        List<City> result = characterTree.filterBy("mo");
        assertTrue(!result.isEmpty());
    }

    private List<City> loadRealData() throws IOException {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(City.class, new CityTypeAdapter())
                .create();
        List<City> data = new ArrayList<>(210000);
        InputStream stream = new FileInputStream("app/src/main/res/raw/cities.json");
        JsonReader reader = new JsonReader(new InputStreamReader(stream, "UTF-8"));
        reader.beginArray();
        while (reader.hasNext()) {
            City city = gson.fromJson(reader, City.class);
            data.add(city);
        }
        reader.close();

        Collections.sort(data);
        return data;
    }
}